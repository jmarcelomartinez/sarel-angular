import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultaComponent } from "./consulta/consulta.component";
import { InicioComponent } from "./inicio/inicio.component";
import { CreacionComponent } from "./creacion/creacion.component";
import { EdicionComponent } from "./edicion/edicion.component";
const routes: Routes = [
  {
    path: 'inicio',
    component: InicioComponent
  },
  {
    path: 'consulta',
    component: ConsultaComponent
  },
  {
    path: 'creacion',
    component: CreacionComponent
  },
  {
    path: 'edicion/:id',
    component: EdicionComponent
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
