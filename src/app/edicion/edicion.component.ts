import { Component, OnInit } from '@angular/core';
import { ApiRestService } from '../services/api-rest.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-edicion',
  templateUrl: './edicion.component.html',
  styleUrls: ['./edicion.component.css']
})
export class EdicionComponent implements OnInit {

  id = this.actRoute.snapshot.params['id'];
  zoneData: any = {};

  constructor(
    public restApi: ApiRestService,
    public actRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.restApi.getZoneById(this.id).subscribe((data: {}) => {
      this.zoneData = data;
      this.zoneData.idZone = this.id;
    });
  }

  // Actualiza los datos del tzone
  updateZone() {
    if (window.confirm('¿Está seguro que desea actualizar?')) {
      console.log(this.zoneData)
      this.restApi.updateZone(this.zoneData).subscribe(async (data) => {
        console.log(data)
        this.router.navigate(["consulta"])
      });
    }
  }

}
