import { Component, OnInit } from '@angular/core';
import { ApiRestService } from "./../services/api-rest.service";
@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.scss']
})
export class ConsultaComponent implements OnInit {
  listZones: any = [];
  constructor(
    private restApi: ApiRestService
  ) { }

  ngOnInit(): void {
    this.consultarZones()
  }

  consultarZones() {
    this.restApi.getZones().subscribe((data: {}) => {
      this.listZones=data;
    })
  }
  eliminarZone(id) {
    if (window.confirm('¿Está seguro que desea eliminar el registro?')) {
      this.restApi.deleteZone(id).subscribe((data: {}) => {
        this.consultarZones();
        console.log(data)
      })
    }
  }

}
