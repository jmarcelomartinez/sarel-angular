import { Component, OnInit, Input } from '@angular/core';
import { ApiRestService } from "./../services/api-rest.service";
import { Router } from '@angular/router';
@Component({
  selector: 'app-creacion',
  templateUrl: './creacion.component.html',
  styleUrls: ['./creacion.component.css']
})
export class CreacionComponent implements OnInit {

  @Input() zone = { name: ''};

  constructor(
    private restApi : ApiRestService,
    private router : Router
  ) { }

  ngOnInit(): void {
  }

  ingresarZone() {
    this.restApi.createZone(this.zone).subscribe((data: {}) => {
      this.router.navigate(['/consulta']);
    });
  }

}
