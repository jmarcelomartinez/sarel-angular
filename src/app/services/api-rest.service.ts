import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from "@angular/common/http";
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Zone } from '../models/zone';
@Injectable({
  providedIn: 'root'
})
export class ApiRestService {

  apiURL = 'http://localhost:8080/sarelWeb/rest/api';
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getZones():Observable<Zone>{
    return this.http.get<Zone>(this.apiURL+'/zones').pipe(retry(1), catchError(this.handleError))
  }
  deleteZone(id) {
    return this.http.delete<Zone>(this.apiURL + '/zone/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

// HttpClient API get() method => Consulta un tipoLugar
getZoneById(id): Observable<Zone> {
  return this.http.get<Zone>(this.apiURL + '/zone/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
}

// HttpClient API post() method => Crear tipoLugar
createZone(zone): Observable<Zone> {
  return this.http.post<Zone>(this.apiURL + '/zone', JSON.stringify(zone), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
}

  updateZone(zone): Observable<Zone> {
    return this.http.put<Zone>(this.apiURL + '/zone', JSON.stringify(zone), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
